# InstaMonitor #

A library that tracks the period of time for which the application was open, and the period of time spent on each Activity and Fragment.

## Usage ##

### Application and Activities ###
In order to track time spent on your Application and Activities, use the *BaseApplication* class or a subclass as your application name in your Manifest.


```
#!xml

<application
        android:name="com.instabug.instamonitorlib.presentation.base.BaseApplication"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:theme="@style/AppTheme">
</application>
```

### Fragments ###
In order to track your fragments, just inherit the *BaseFragment* in your fragments.

```
#!Java

public class MyFragment extends BaseFragment { ...
```



### General usage ###
If you need to manage time tracking all by yourself, you can use the *ComponentsManager* to do all the steps.

```
#!Java

ComponentsManager componentsManager = new ComponentsManager(mContext);
componentsManager.resetStartTime(componentName);
```

## Docs ##
You will find the library's JavaDocs under *instamonitorlib/docs* folder