package com.instabug.instamonitorlib.presentation.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.instabug.instamonitorlib.managers.ComponentsManager;

/**
 * Base Fragment that all Fragments should inherit from.
 */
public class BaseFragment extends Fragment {
    private ComponentsManager mComponentsManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mComponentsManager = new ComponentsManager(getContext());
        mComponentsManager.resetStartTime(getClass().getSimpleName());
    }

    @Override
    public void onStart() {
        super.onStart();
        mComponentsManager.resetStartTime(getClass().getSimpleName());
    }

    @Override
    public void onPause() {
        super.onPause();
        mComponentsManager.saveTimeSpent(getClass().getSimpleName());
    }

    @Override
    public void onStop() {
        super.onStop();
        mComponentsManager.saveTimeSpent(getClass().getSimpleName());
    }
}