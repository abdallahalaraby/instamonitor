package com.instabug.instamonitorlib.presentation.base;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.instabug.instamonitorlib.managers.ComponentsManager;

/**
 * Application class that must be used by the application tag in the app manifest either directly or
 * through a subclass.
 */
public class BaseApplication extends Application implements Application.ActivityLifecycleCallbacks {
    private ComponentsManager mComponentsManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponentsManager = new ComponentsManager(this);
        mComponentsManager.resetStartTime(getClass().getSimpleName());
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        mComponentsManager.resetStartTime(activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {
        mComponentsManager.saveTimeSpent(activity.getClass().getSimpleName());
        mComponentsManager.saveTimeSpent(getClass().getSimpleName());
    }

    @Override
    public void onActivityStopped(Activity activity) {
        mComponentsManager.saveTimeSpent(activity.getClass().getSimpleName());
        mComponentsManager.saveTimeSpent(getClass().getSimpleName());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        mComponentsManager.saveTimeSpent(activity.getClass().getSimpleName());
    }
}