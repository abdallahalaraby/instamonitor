package com.instabug.instamonitorlib.managers;

import android.content.Context;

import com.instabug.instamonitorlib.domain.interactors.GetAllComponentsInteractor;
import com.instabug.instamonitorlib.domain.interactors.impl.ClearTracksInteractorImpl;
import com.instabug.instamonitorlib.domain.interactors.impl.GetAllComponentsInteractorImpl;
import com.instabug.instamonitorlib.domain.interactors.impl.ResetStartTimeInteractorImpl;
import com.instabug.instamonitorlib.domain.interactors.impl.SaveTimeSpentInteractorImpl;
import com.instabug.instamonitorlib.domain.interactors.impl.StartTrackingInteractorImpl;
import com.instabug.instamonitorlib.domain.interactors.impl.StopTrackingInteractorImpl;
import com.instabug.instamonitorlib.domain.repository.ComponentsRepository;
import com.instabug.instamonitorlib.storage.ComponentsRepositoryImpl;
import com.instabug.instamonitorlib.threading.MainThreadImpl;
import com.instabug.instamonitorlib.threading.WorkerThreadImpl;

/**
 * Manager class that manages the Components time tracking operations.
 */
public class ComponentsManager {
    private ComponentsRepository mComponentsRepository;

    public ComponentsManager(Context context) {
        mComponentsRepository = new ComponentsRepositoryImpl(context);
    }

    public void saveTimeSpent(String componentName) {
        new SaveTimeSpentInteractorImpl(WorkerThreadImpl.getInstance(),
                MainThreadImpl.getInstance(),
                mComponentsRepository,
                componentName).execute();
    }

    public void resetStartTime(String componentName) {
        new ResetStartTimeInteractorImpl(WorkerThreadImpl.getInstance(),
                MainThreadImpl.getInstance(),
                mComponentsRepository,
                componentName).execute();
    }

    public void getAllComponents(GetAllComponentsInteractor.Callback callback) {
        new GetAllComponentsInteractorImpl(WorkerThreadImpl.getInstance(),
                MainThreadImpl.getInstance(),
                mComponentsRepository,
                callback).execute();
    }

    public void startTracking(String componentName) {
        new StartTrackingInteractorImpl(WorkerThreadImpl.getInstance(),
                MainThreadImpl.getInstance(),
                mComponentsRepository,
                componentName).execute();
    }

    public void stopTracking(String componentName) {
        new StopTrackingInteractorImpl(WorkerThreadImpl.getInstance(),
                MainThreadImpl.getInstance(),
                mComponentsRepository,
                componentName).execute();
    }

    public void clearAllTracks() {
        new ClearTracksInteractorImpl(WorkerThreadImpl.getInstance(),
                MainThreadImpl.getInstance(),
                mComponentsRepository).execute();
    }
}