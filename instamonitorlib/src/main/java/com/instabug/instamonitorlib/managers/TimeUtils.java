package com.instabug.instamonitorlib.managers;

import android.os.SystemClock;

/**
 * A utility class that is used to retrieve and make calculations on Time.
 */
public class TimeUtils {
    private static TimeUtils sInstance;

    private TimeUtils() {

    }

    public static TimeUtils getInstance() {
        if (sInstance == null) {
            synchronized (TimeUtils.class) {
                if (sInstance == null) {
                    sInstance = new TimeUtils();
                }
            }
        }
        return sInstance;
    }

    /**
     * Returns the time since booting the device
     * @return up time in milliseconds
     */
    public long getCurrentTimeMillis() {
        return SystemClock.uptimeMillis();
    }

    /**
     * Calculates the time spent from a given time until current time.
     * @param startTimeMillis
     * @return the time spent from start until now.
     */
    public long getTimeSpentMillis(long startTimeMillis) {
        long timeSpent = 0;
        if (startTimeMillis > 0) {
            timeSpent = getCurrentTimeMillis() - startTimeMillis;
        }
        return timeSpent;
    }
}