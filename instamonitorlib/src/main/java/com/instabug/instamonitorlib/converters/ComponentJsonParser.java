package com.instabug.instamonitorlib.converters;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.instabug.instamonitorlib.domain.model.Component;

/**
 * Converts from Component to JSON String and vice versa.
 */
public class ComponentJsonParser {
    private static ComponentJsonParser sInstance;
    private Gson mGson;

    private ComponentJsonParser() {
        mGson = new Gson();
    }

    public static ComponentJsonParser getInstance() {
        if (sInstance == null) {
            synchronized (ComponentJsonParser.class) {
                if (sInstance == null) {
                    sInstance = new ComponentJsonParser();
                }
            }
        }
        return sInstance;
    }

    /**
     * Creates JSON Object of the type Component from a String.
     *
     * @param componentStr
     * @return
     */
    public Component toComponentObject(String componentStr) {
        Component component = null;
        try {
            component = mGson.fromJson(componentStr, Component.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return component;
    }

    /**
     * Converts a Component to a JSON String.
     *
     * @param component
     * @return
     */
    public String toComponentString(Component component) {
        return mGson.toJson(component);
    }
}