package com.instabug.instamonitorlib.domain.interactors;

import com.instabug.instamonitorlib.domain.interactors.base.Interactor;

public interface SaveTimeSpentInteractor extends Interactor {

    void performSaveTimeSpent(String componentName);
}