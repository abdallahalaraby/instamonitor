package com.instabug.instamonitorlib.domain.interactors;

import com.instabug.instamonitorlib.domain.interactors.base.Interactor;
import com.instabug.instamonitorlib.domain.model.Component;

import java.util.List;

public interface GetAllComponentsInteractor extends Interactor {

    List<Component> performGetAllComponents();

    interface Callback {
        void onComponentsRetrieved(List<Component> components);
    }
}