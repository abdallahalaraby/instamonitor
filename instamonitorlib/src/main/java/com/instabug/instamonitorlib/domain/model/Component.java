package com.instabug.instamonitorlib.domain.model;

public class Component implements Comparable<Component> {
    public String name = "";
    public long startTime = 0;
    public long timeSpent = 0;
    public boolean tracking = true;

    public Component(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Component another) {
        return this.name.compareTo(another.name);
    }
}