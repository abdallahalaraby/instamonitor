package com.instabug.instamonitorlib.domain.interactors.base;

import com.instabug.instamonitorlib.domain.executor.MainThread;
import com.instabug.instamonitorlib.domain.executor.WorkerThread;

public abstract class AbstractInteractor implements Interactor {
    protected WorkerThread mWorkerThread;
    protected MainThread mMainThread;

    public AbstractInteractor(WorkerThread workerExecutor,
                              MainThread mainThread) {
        mWorkerThread = workerExecutor;
        mMainThread = mainThread;
    }

    /**
     * Main interactor logic.
     */
    public abstract void run();

    /**
     * Performs operations before the interactor is executed.
     */
    public final void onPreExecute() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                onStarted();
            }
        });
    }

    /**
     * Called when the interactor is about to run.
     */
    public void onStarted() {

    }

    /**
     * Finishes the interactor and returns to the main thread.
     */
    public final void onPostExecute() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                onFinished();
            }
        });
    }

    /**
     * Called after the interactor is executed.
     */
    public void onFinished() {

    }

    @Override
    public final void execute() {
        mWorkerThread.execute(this);
    }
}