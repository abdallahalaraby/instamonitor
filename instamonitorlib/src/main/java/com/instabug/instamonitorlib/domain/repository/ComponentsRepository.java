package com.instabug.instamonitorlib.domain.repository;

import com.instabug.instamonitorlib.domain.model.Component;

import java.util.List;

/**
 * Storing and retrieving components.
 */
public interface ComponentsRepository {
    void resetStartTime(String componentName);

    void saveTimeSpentSinceStarted(String componentName);

    void startTracking(String componentName);

    void stopTracking(String componentName);

    List<Component> getAllComponents();

    void clearAllTracks();
}