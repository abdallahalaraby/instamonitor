package com.instabug.instamonitorlib.domain.interactors.impl;

import com.instabug.instamonitorlib.domain.executor.MainThread;
import com.instabug.instamonitorlib.domain.executor.WorkerThread;
import com.instabug.instamonitorlib.domain.interactors.ClearTracksInteractor;
import com.instabug.instamonitorlib.domain.interactors.base.AbstractInteractor;
import com.instabug.instamonitorlib.domain.repository.ComponentsRepository;

public class ClearTracksInteractorImpl extends AbstractInteractor implements ClearTracksInteractor {
    private ComponentsRepository mComponentsRepository;

    public ClearTracksInteractorImpl(WorkerThread workerExecutor,
                                     MainThread mainThread,
                                     ComponentsRepository componentsRepository) {
        super(workerExecutor, mainThread);
        mComponentsRepository = componentsRepository;
    }

    @Override
    public void run() {
        performClearTracks();
    }

    @Override
    public void performClearTracks() {
        mComponentsRepository.clearAllTracks();
    }
}