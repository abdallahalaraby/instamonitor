package com.instabug.instamonitorlib.domain.interactors.impl;

import com.instabug.instamonitorlib.domain.executor.MainThread;
import com.instabug.instamonitorlib.domain.executor.WorkerThread;
import com.instabug.instamonitorlib.domain.interactors.StartTrackingInteractor;
import com.instabug.instamonitorlib.domain.interactors.base.AbstractInteractor;
import com.instabug.instamonitorlib.domain.repository.ComponentsRepository;

public class StartTrackingInteractorImpl extends AbstractInteractor implements StartTrackingInteractor {
    private String mComponentName;
    private ComponentsRepository mComponentsRepository;

    public StartTrackingInteractorImpl(WorkerThread workerExecutor,
                                       MainThread mainThread,
                                       ComponentsRepository componentsRepository,
                                       String componentName) {
        super(workerExecutor, mainThread);
        mComponentsRepository = componentsRepository;
        mComponentName = componentName;
    }

    @Override
    public void run() {
        performStartTracking(mComponentName);
    }

    @Override
    public void performStartTracking(String componentName) {
        mComponentsRepository.startTracking(componentName);
    }
}