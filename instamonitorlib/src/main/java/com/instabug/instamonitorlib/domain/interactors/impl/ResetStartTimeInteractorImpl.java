package com.instabug.instamonitorlib.domain.interactors.impl;

import com.instabug.instamonitorlib.domain.executor.MainThread;
import com.instabug.instamonitorlib.domain.executor.WorkerThread;
import com.instabug.instamonitorlib.domain.interactors.ResetStartTimeInteractor;
import com.instabug.instamonitorlib.domain.interactors.base.AbstractInteractor;
import com.instabug.instamonitorlib.domain.repository.ComponentsRepository;

public class ResetStartTimeInteractorImpl extends AbstractInteractor implements ResetStartTimeInteractor {
    private String mComponentName;
    private ComponentsRepository mComponentsRepository;

    public ResetStartTimeInteractorImpl(WorkerThread workerExecutor,
                                        MainThread mainThread,
                                        ComponentsRepository componentsRepository,
                                        String componentName) {
        super(workerExecutor, mainThread);
        mComponentsRepository = componentsRepository;
        mComponentName = componentName;
    }

    @Override
    public void run() {
        performResetStartTime(mComponentName);
    }

    @Override
    public void performResetStartTime(String componentName) {
        mComponentsRepository.resetStartTime(componentName);
    }
}