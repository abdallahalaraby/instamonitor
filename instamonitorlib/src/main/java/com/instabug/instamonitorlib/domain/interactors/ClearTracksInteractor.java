package com.instabug.instamonitorlib.domain.interactors;

import com.instabug.instamonitorlib.domain.interactors.base.Interactor;

public interface ClearTracksInteractor extends Interactor {

    void performClearTracks();
}