package com.instabug.instamonitorlib.domain.interactors.base;

/**
 * An Interactor implementation represents a single use case.
 */
public interface Interactor {

    /**
     * This method starts the interactor and run it on a worker thread.
     */
    void execute();
}