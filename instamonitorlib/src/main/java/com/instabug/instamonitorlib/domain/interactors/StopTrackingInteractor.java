package com.instabug.instamonitorlib.domain.interactors;

import com.instabug.instamonitorlib.domain.interactors.base.Interactor;

public interface StopTrackingInteractor extends Interactor {

    void performStopTracking(String componentName);
}