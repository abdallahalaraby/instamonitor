package com.instabug.instamonitorlib.domain.interactors.impl;

import com.instabug.instamonitorlib.domain.executor.MainThread;
import com.instabug.instamonitorlib.domain.executor.WorkerThread;
import com.instabug.instamonitorlib.domain.interactors.GetAllComponentsInteractor;
import com.instabug.instamonitorlib.domain.interactors.base.AbstractInteractor;
import com.instabug.instamonitorlib.domain.model.Component;
import com.instabug.instamonitorlib.domain.repository.ComponentsRepository;

import java.util.List;

public class GetAllComponentsInteractorImpl extends AbstractInteractor implements GetAllComponentsInteractor {
    private ComponentsRepository mComponentsRepository;
    private Callback mCallback;
    private List<Component> mComponents;

    public GetAllComponentsInteractorImpl(WorkerThread workerExecutor,
                                          MainThread mainThread,
                                          ComponentsRepository componentsRepository,
                                          Callback callback) {
        super(workerExecutor, mainThread);
        mComponentsRepository = componentsRepository;
        mCallback = callback;
    }

    @Override
    public void run() {
        mComponents = performGetAllComponents();
    }

    @Override
    public List<Component> performGetAllComponents() {
        return mComponentsRepository.getAllComponents();
    }

    @Override
    public void onFinished() {
        super.onFinished();
        mCallback.onComponentsRetrieved(mComponents);
    }
}