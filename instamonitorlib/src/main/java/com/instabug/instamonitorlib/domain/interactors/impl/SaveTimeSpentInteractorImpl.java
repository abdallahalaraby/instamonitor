package com.instabug.instamonitorlib.domain.interactors.impl;

import com.instabug.instamonitorlib.domain.executor.MainThread;
import com.instabug.instamonitorlib.domain.executor.WorkerThread;
import com.instabug.instamonitorlib.domain.interactors.SaveTimeSpentInteractor;
import com.instabug.instamonitorlib.domain.interactors.base.AbstractInteractor;
import com.instabug.instamonitorlib.domain.repository.ComponentsRepository;

public class SaveTimeSpentInteractorImpl extends AbstractInteractor implements SaveTimeSpentInteractor {
    private String mComponentName;
    private ComponentsRepository mComponentsRepository;

    public SaveTimeSpentInteractorImpl(WorkerThread workerExecutor,
                                       MainThread mainThread,
                                       ComponentsRepository componentsRepository,
                                       String componentName) {
        super(workerExecutor, mainThread);
        mComponentsRepository = componentsRepository;
        mComponentName = componentName;
    }

    @Override
    public void run() {
        performSaveTimeSpent(mComponentName);
    }

    @Override
    public void performSaveTimeSpent(String componentName) {
        mComponentsRepository.saveTimeSpentSinceStarted(componentName);
    }
}