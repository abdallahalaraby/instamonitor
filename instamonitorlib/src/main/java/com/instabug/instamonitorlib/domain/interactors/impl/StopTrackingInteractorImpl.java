package com.instabug.instamonitorlib.domain.interactors.impl;

import com.instabug.instamonitorlib.domain.executor.MainThread;
import com.instabug.instamonitorlib.domain.executor.WorkerThread;
import com.instabug.instamonitorlib.domain.interactors.StopTrackingInteractor;
import com.instabug.instamonitorlib.domain.interactors.base.AbstractInteractor;
import com.instabug.instamonitorlib.domain.repository.ComponentsRepository;

public class StopTrackingInteractorImpl extends AbstractInteractor implements StopTrackingInteractor {
    private String mComponentName;
    private ComponentsRepository mComponentsRepository;

    public StopTrackingInteractorImpl(WorkerThread workerExecutor,
                                      MainThread mainThread,
                                      ComponentsRepository componentsRepository,
                                      String componentName) {
        super(workerExecutor, mainThread);
        mComponentsRepository = componentsRepository;
        mComponentName = componentName;
    }

    @Override
    public void run() {
        performStopTracking(mComponentName);
    }

    @Override
    public void performStopTracking(String componentName) {
        mComponentsRepository.stopTracking(componentName);
    }
}