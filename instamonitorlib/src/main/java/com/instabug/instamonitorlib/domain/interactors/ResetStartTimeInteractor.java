package com.instabug.instamonitorlib.domain.interactors;

import com.instabug.instamonitorlib.domain.interactors.base.Interactor;

public interface ResetStartTimeInteractor extends Interactor {

    void performResetStartTime(String componentName);
}