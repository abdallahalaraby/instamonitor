package com.instabug.instamonitorlib.domain.executor;

import com.instabug.instamonitorlib.domain.interactors.base.AbstractInteractor;

/**
 * This executor is responsible for running interactors on background threads.
 */
public interface WorkerThread {

    /**
     * This method should call the interactor's run method and thus start the interactor. This should be called
     * on a background thread as interactors might do lengthy operations.
     *
     * @param interactor The interactor to run.
     */
    void execute(final AbstractInteractor interactor);
}