package com.instabug.instamonitorlib.domain.interactors;

import com.instabug.instamonitorlib.domain.interactors.base.Interactor;

public interface StartTrackingInteractor extends Interactor {

    void performStartTracking(String componentName);
}