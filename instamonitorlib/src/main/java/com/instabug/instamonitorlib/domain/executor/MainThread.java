package com.instabug.instamonitorlib.domain.executor;

/**
 * This interface's implementation will enable Interactors to run some code on the UI Thread.
 */
public interface MainThread {

    /**
     * Allows runnable to run in the UI Thread.
     *
     * @param runnable The Runnable to execute on the UI Thread.
     */
    void post(final Runnable runnable);
}