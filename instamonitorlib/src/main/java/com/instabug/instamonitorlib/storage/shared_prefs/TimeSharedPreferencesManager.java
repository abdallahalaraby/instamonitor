package com.instabug.instamonitorlib.storage.shared_prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.instabug.instamonitorlib.domain.model.Component;
import com.instabug.instamonitorlib.converters.ComponentJsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Manages storing and retrieving data from SharedPreferences.
 */
public class TimeSharedPreferencesManager {
    private final String prefsFileName = "instamonitor_time";
    private SharedPreferences mSharedPreferences;

    public TimeSharedPreferencesManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
    }

    /**
     * Saves the start time of tracking a component, so we can use it later for calculations
     * @param componentName
     * @param startTimeMillis
     */
    public void saveStartTime(String componentName, long startTimeMillis) {
        Component component = getComponent(componentName);
        if (component.tracking) {
            component.startTime = startTimeMillis;
            saveComponent(component);
        }
    }

    /**
     * Saves the time spent on a component. The saved value is accumulative, which means it will not
     * reset its value if you close the app and open it again.
     * @param componentName
     * @param timeSpent
     */
    public void saveTimeSpent(String componentName, long timeSpent) {
        Component component = getComponent(componentName);
        if (component.tracking) {
            component.timeSpent += timeSpent;
            saveComponent(component);
        }
    }

    /**
     * Enables/Disables the tracking value for a component.
     * In order to keep track of the time spent on a component if it is tracked.
     * @param componentName
     * @param tracking
     */
    public void setTracking(String componentName, boolean tracking) {
        Component component = getComponent(componentName);
        component.tracking = tracking;
        if (!tracking) {
            component.timeSpent = 0;
        }
        saveComponent(component);
    }

    /**
     * Saves a component as a JSON String in the SharedPreferences.
     * @param component
     */
    private void saveComponent(Component component) {
        String componentJson = ComponentJsonParser.getInstance().toComponentString(component);
        mSharedPreferences.edit().putString(component.name, componentJson).apply();
    }

    /**
     * Gets a component and parses it from String to Component object.
     * @param componentName
     * @return
     */
    public Component getComponent(String componentName) {
        String componentStr = mSharedPreferences.getString(componentName, null);
        Component component = ComponentJsonParser.getInstance().toComponentObject(componentStr);
        if (component == null) {
            component = new Component(componentName);
        }
        return component;
    }

    /**
     * Retrieves all available components.
     * @return
     */
    public List<Component> getAllComponents() {
        List<Component> components = new ArrayList<>();

        Map<String, ?> allStoredData = mSharedPreferences.getAll();
        for (String key : allStoredData.keySet()) {
            try {
                String value = (String) allStoredData.get(key);
                Component component = ComponentJsonParser.getInstance().toComponentObject(value);
                if (component != null) {
                    components.add(component);
                }
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }

        return components;
    }

    /**
     * Clears all components. but keeps the components' names stored.
     */
    public void clearAll() {
        List<Component> components = getAllComponents();
        for (Component component : components) {
            Component newComponent = new Component(component.name);
            saveComponent(newComponent);
        }
    }
}