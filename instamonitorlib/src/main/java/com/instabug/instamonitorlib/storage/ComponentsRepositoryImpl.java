package com.instabug.instamonitorlib.storage;

import android.content.Context;

import com.instabug.instamonitorlib.managers.TimeUtils;
import com.instabug.instamonitorlib.domain.repository.ComponentsRepository;
import com.instabug.instamonitorlib.domain.model.Component;
import com.instabug.instamonitorlib.storage.shared_prefs.TimeSharedPreferencesManager;

import java.util.List;

/**
 * Class for handling saving and retrieving the time tracking for components.
 */
public class ComponentsRepositoryImpl implements ComponentsRepository {
    private TimeSharedPreferencesManager mTimeSharedPreferencesManager;

    public ComponentsRepositoryImpl(Context context) {
        mTimeSharedPreferencesManager = new TimeSharedPreferencesManager(context);
    }

    /**
     * Resets the start time to begin counting.
     */
    @Override
    public void resetStartTime(String componentName) {
        long startTimeMillis = TimeUtils.getInstance().getCurrentTimeMillis();
        mTimeSharedPreferencesManager.saveStartTime(componentName, startTimeMillis);
    }

    /**
     * Saves the time spent since the start time is last reset.
     *
     * @param componentName
     */
    @Override
    public void saveTimeSpentSinceStarted(String componentName) {
        Component component = mTimeSharedPreferencesManager.getComponent(componentName);
        long startTimeMillis = component.startTime;
        long timeSpent = TimeUtils.getInstance().getTimeSpentMillis(startTimeMillis);
        mTimeSharedPreferencesManager.saveTimeSpent(componentName, timeSpent);
        resetStartTime(componentName);
    }

    /**
     * Allows time tracking for a component.
     *
     * @param componentName
     */
    @Override
    public void startTracking(String componentName) {
        mTimeSharedPreferencesManager.setTracking(componentName, true);
    }

    /**
     * Stops time tracking for a component.
     *
     * @param componentName
     */
    @Override
    public void stopTracking(String componentName) {
        mTimeSharedPreferencesManager.setTracking(componentName, false);
    }

    /**
     * Returns all components from storage.
     *
     * @return
     */
    @Override
    public List<Component> getAllComponents() {
        return mTimeSharedPreferencesManager.getAllComponents();
    }

    /**
     * Resets all tracks to zero values.
     */
    @Override
    public void clearAllTracks() {
        mTimeSharedPreferencesManager.clearAll();
    }
}