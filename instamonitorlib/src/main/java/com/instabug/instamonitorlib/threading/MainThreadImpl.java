package com.instabug.instamonitorlib.threading;

import android.os.Handler;
import android.os.Looper;

import com.instabug.instamonitorlib.domain.executor.MainThread;

/**
 * This class runs Runnables on the UI thread.
 */
public class MainThreadImpl implements MainThread {
    private static volatile MainThread sMainThread;
    private Handler mHandler;

    private MainThreadImpl() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    public static MainThread getInstance() {
        if (sMainThread == null) {
            synchronized (MainThreadImpl.class) {
                if (sMainThread == null) {
                    sMainThread = new MainThreadImpl();
                }
            }
        }

        return sMainThread;
    }

    @Override
    public void post(Runnable runnable) {
        mHandler.post(runnable);
    }
}