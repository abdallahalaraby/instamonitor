package com.instabug.instamonitorlib.threading;

import android.os.AsyncTask;

import com.instabug.instamonitorlib.domain.executor.WorkerThread;
import com.instabug.instamonitorlib.domain.interactors.base.AbstractInteractor;

/**
 * This class prepares the thread pool to accept certain number of Runnables at the same time, and it executes the
 * interactor's methods in order on a worker thread.
 */
public class WorkerThreadImpl implements WorkerThread {
    private static volatile WorkerThread sWorkerThread;

    private WorkerThreadImpl() {
    }

    public static WorkerThread getInstance() {
        if (sWorkerThread == null) {
            synchronized (WorkerThreadImpl.class) {
                if (sWorkerThread == null) {
                    sWorkerThread = new WorkerThreadImpl();
                }
            }
        }

        return sWorkerThread;
    }

    @Override
    public void execute(final AbstractInteractor interactor) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                interactor.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                interactor.run();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                interactor.onPostExecute();
            }
        }.execute();
    }
}