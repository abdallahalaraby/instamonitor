package com.instabug.instamonitorapp.presentation.presenters.impl;

import com.instabug.instamonitorapp.presentation.presenters.LogsActivityPresenter;
import com.instabug.instamonitorapp.presentation.views.activities.LogsActivity;
import com.instabug.instamonitorlib.domain.interactors.GetAllComponentsInteractor;
import com.instabug.instamonitorlib.domain.model.Component;
import com.instabug.instamonitorlib.managers.ComponentsManager;
import com.instabug.instamonitorlib.presentation.base.BaseApplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LogsActivityPresenterImpl implements LogsActivityPresenter {
    private LogsActivityPresenter.View mView;
    private ComponentsManager mComponentsManager;

    public LogsActivityPresenterImpl(LogsActivityPresenter.View view) {
        mView = view;
        mComponentsManager = new ComponentsManager(mView.getContext());
        mComponentsManager.stopTracking(LogsActivity.class.getSimpleName());
    }

    @Override
    public void loadComponents() {
        mComponentsManager.getAllComponents(new GetAllComponentsInteractor.Callback() {
            @Override
            public void onComponentsRetrieved(List<Component> components) {
                List<Component> filteredComponents = filterTrackedComponents(components);
                Collections.sort(filteredComponents);
                mView.addComponentsToListView(filteredComponents);
            }
        });
    }

    @Override
    public void resetComponents() {
        mComponentsManager.clearAllTracks();
        mComponentsManager.stopTracking(LogsActivity.class.getSimpleName());
        mComponentsManager.resetStartTime(BaseApplication.class.getSimpleName());
        loadComponents();
    }

    @Override
    public void requestDataFromStorage() {
        mComponentsManager.getAllComponents(new GetAllComponentsInteractor.Callback() {
            @Override
            public void onComponentsRetrieved(List<Component> components) {
                mView.filter(components);
            }
        });
    }

    /**
     * Filters the tracked components only.
     *
     * @param components
     * @return
     */
    private List<Component> filterTrackedComponents(List<Component> components) {
        List<Component> filteredComponents = new ArrayList<>();
        for (Component component : components) {
            if (component.tracking) {
                filteredComponents.add(component);
            }
        }
        return filteredComponents;
    }
}