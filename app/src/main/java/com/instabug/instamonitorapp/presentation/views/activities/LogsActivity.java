package com.instabug.instamonitorapp.presentation.views.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.instabug.instamonitorapp.R;
import com.instabug.instamonitorapp.presentation.presenters.LogsActivityPresenter;
import com.instabug.instamonitorapp.presentation.presenters.impl.LogsActivityPresenterImpl;
import com.instabug.instamonitorapp.presentation.views.adapters.ResultsAdapter;
import com.instabug.instamonitorapp.presentation.views.dialogs.FilterDialog;
import com.instabug.instamonitorlib.domain.model.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LogsActivity extends AppCompatActivity implements View.OnClickListener, LogsActivityPresenter.View {
    private ListView mLV_Results;
    private List<Component> mComponents;
    private ResultsAdapter mAdapter;
    private LogsActivityPresenter mPresenter;
    private FilterDialog mFilterDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);
        mPresenter = new LogsActivityPresenterImpl(this);

        mLV_Results = ((ListView) findViewById(R.id.lv_results));
        findViewById(R.id.b_filter).setOnClickListener(this);
        findViewById(R.id.b_reset).setOnClickListener(this);
        mComponents = new ArrayList<>();
        mAdapter = new ResultsAdapter(mComponents);
        mLV_Results.setAdapter(mAdapter);
        mFilterDialog = new FilterDialog(this, new FilterDialog.Callback() {
            @Override
            public void onDialogDismiss() {
                mPresenter.loadComponents();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.loadComponents();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.b_filter:
                mPresenter.requestDataFromStorage();
                break;
            case R.id.b_reset:
                reset();
                break;
        }
    }

    @Override
    public void filter(List<Component> allComponents) {
        for (Component component : allComponents) {
            if (component.name.equals(LogsActivity.class.getSimpleName())) {
                allComponents.remove(component);
                break;
            }
        }
        Collections.sort(allComponents);
        mFilterDialog.show(allComponents);
    }

    private void reset() {
        mPresenter.resetComponents();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void addComponentsToListView(List<Component> components) {
        mComponents.clear();
        mComponents.addAll(components);
        mAdapter.notifyDataSetChanged();
    }
}