package com.instabug.instamonitorapp.presentation.views.dialogs;

import android.content.Context;
import android.content.DialogInterface;

import com.afollestad.materialdialogs.MaterialDialog;
import com.instabug.instamonitorapp.R;
import com.instabug.instamonitorlib.domain.model.Component;
import com.instabug.instamonitorlib.managers.ComponentsManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilterDialog {
    private ComponentsManager mComponentsManager;
    private Context mContext;
    private Callback mCallback;
    private String[] mItemsNames;
    private Integer[] mTrackedIndexes;

    public FilterDialog(Context context, Callback callback) {
        mContext = context;
        mCallback = callback;
        mComponentsManager = new ComponentsManager(context);
    }

    public void show(final List<Component> components) {
        initItems(components);
        new MaterialDialog.Builder(mContext)
                .title(R.string.logs_filter)
                .items(mItemsNames)
                .itemsCallbackMultiChoice(mTrackedIndexes, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        saveSelections(components, which);
                        return true;
                    }
                })
                .positiveText(R.string.filters_save)
                .show().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mCallback.onDialogDismiss();
            }
        });
    }

    private void initItems(List<Component> components) {
        List<Integer> mTrackedIndexesList = new ArrayList<>();
        mItemsNames = new String[components.size()];
        for (int i = 0; i < components.size(); i++) {
            mItemsNames[i] = components.get(i).name;
            Component component = components.get(i);
            if (component.tracking) {
                mTrackedIndexesList.add(i);
            }
        }
        mTrackedIndexes = mTrackedIndexesList.toArray(new Integer[mTrackedIndexesList.size()]);
    }

    /**
     * Saves the new tracking values for all available Components.
     *
     * @param components
     * @param which
     */
    private void saveSelections(List<Component> components, Integer[] which) {
        List<Integer> selectedIndexes = new ArrayList<>();
        selectedIndexes.addAll(Arrays.asList(which));
        for (int i = 0; i < components.size(); i++) {
            Component component = components.get(i);
            if (selectedIndexes.contains(i)) {
                mComponentsManager.startTracking(component.name);
            } else {
                mComponentsManager.stopTracking(component.name);
            }
        }
    }

    public interface Callback {
        void onDialogDismiss();
    }
}