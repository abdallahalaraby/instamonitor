package com.instabug.instamonitorapp.presentation.presenters;

import android.content.Context;

import com.instabug.instamonitorlib.domain.model.Component;

import java.util.List;

public interface LogsActivityPresenter {

    void loadComponents();

    void resetComponents();

    void requestDataFromStorage();

    interface View {

        Context getContext();

        void filter(List<Component> allComponents);

        void addComponentsToListView(List<Component> components);
    }
}