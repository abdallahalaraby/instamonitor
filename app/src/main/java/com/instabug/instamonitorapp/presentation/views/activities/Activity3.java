package com.instabug.instamonitorapp.presentation.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.instabug.instamonitorapp.R;

public class Activity3 extends BaseSimpleActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        setTitle(R.string.activity_3_title);
        findViewById(R.id.fab_show_metrics).setOnClickListener(this);
        findViewById(R.id.b_activity1).setOnClickListener(this);
        findViewById(R.id.b_activity2).setOnClickListener(this);
        findViewById(R.id.b_activity3).setOnClickListener(this);
    }
}