package com.instabug.instamonitorapp.presentation.views.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.instabug.instamonitorapp.R;

public class BaseSimpleActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_show_metrics:
                loadActivity(LogsActivity.class);
                break;
            case R.id.b_activity1:
                loadActivityAndFinishCurrent(Activity1.class);
                break;
            case R.id.b_activity2:
                loadActivityAndFinishCurrent(Activity2.class);
                break;
            case R.id.b_activity3:
                loadActivityAndFinishCurrent(Activity3.class);
                break;
        }
    }

    private void loadActivity(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    private void loadActivityAndFinishCurrent(Class cls) {
        loadActivity(cls);
        finish();
    }
}