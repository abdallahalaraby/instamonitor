package com.instabug.instamonitorapp.presentation.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.instabug.instamonitorapp.R;
import com.instabug.instamonitorapp.utils.TimeUtils;
import com.instabug.instamonitorlib.domain.model.Component;

import java.util.List;

public class ResultsAdapter extends BaseAdapter {
    private List<Component> mComponents;

    public ResultsAdapter(List<Component> components) {
        mComponents = components;
    }

    @Override
    public int getCount() {
        return mComponents.size();
    }

    @Override
    public Object getItem(int position) {
        return mComponents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_result, parent, false);
            holder = new ViewHolder();
            holder.tv_component_name = ((TextView) convertView.findViewById(R.id.tv_component_name));
            holder.tv_time_spent = ((TextView) convertView.findViewById(R.id.tv_time_spent));
            convertView.setTag(holder);
        } else {
            holder = ((ViewHolder) convertView.getTag());
        }

        Component component = mComponents.get(position);
        holder.tv_component_name.setText(component.name);
        holder.tv_time_spent.setText(TimeUtils.millisToTime(component.timeSpent));

        return convertView;
    }

    static class ViewHolder {
        TextView tv_component_name;
        TextView tv_time_spent;
    }
}