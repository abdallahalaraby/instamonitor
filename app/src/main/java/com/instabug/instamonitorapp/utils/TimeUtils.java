package com.instabug.instamonitorapp.utils;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TimeUtils {

    public static String millisToTime(long millis) {
        String formattedTime = String.format(Locale.ENGLISH, "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );
        return formattedTime;
    }
}